# Exercise #1

1. Create and define RAID 5 using mdadm
2. Check the status of your device

```
mdadm --detail /dev/md0; cat /proc/mdstat
```
 
3. Simulate disk failure

```
mdadm /dev/md0 -f /dev/loop1
```

4. Recover from a disk failure
   - "Remove" the failed disk
   - "Add" new disk to computer
   - Use this disk in existing RAID

[![exercise #1](./ex1.gif)](https://asciinema.org/a/167437)

# Exercise #2

1. Add a hot-spare disk to existing RAID5
2. Fail one disk and watch what will happened
3. Destroy RAID device completely
4. Create a new RAID5 device with only two disks

[![exercise #2](./ex2.gif)](https://asciinema.org/a/167509)
 

## Shared hot-spare disk

 - [Sharing hot spare device in software RAID](https://fivtips.blogspot.cz/2017/03/sharing-hot-spare-device-in-software.html)



# Btrfs backups

- https://forum.rockstor.com/t/using-btrfs-send-receieve-for-backups/1342
- https://www.reddit.com/r/linux/comments/2e2iv0/confused_about_btrfs_incremental_remote_backups/
- https://github.com/AmesCornish/buttersink
