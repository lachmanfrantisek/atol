Asciinema recordings for exercises at course Advanced Topics of Linux Administration by Marek Grac at FI MU Brno.

- [Disk management](./01-disk-management)
- [RAID, Btrfs](./02-raid-btrfs)
- [Encryption](./03-encryption)
- [System](./04-system)
- [ISCSi](./05-iscsi)

