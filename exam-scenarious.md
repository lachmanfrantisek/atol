# Partitioning (20%) o node A
- create a disk device that has physical size (approx 200MB)
- divide the disk into two parts and store 150MB on each of them
    - data can be mostly same on each of these part
- mount these parts at the boot time

# Partitioning on PM (15%)
- Create logical value named lv-first which will have ~100MiB
- Allocate rest of space to logical volume lv-second so 40 extents will be left free for possible extensions

# Data store (40%)
- prepare a data server on node B and serve data, so only node A can connect
- data has to be encrypted during network transfer
- data store (=device) on node A should be mounted read-write to `/mnt/my-data` and write access has to work

# Storage (30%) on the node delta
- Create scenario with following properties
    - When two physical disks are broken, the filesystem on top of them is still working
    - There is an unused disk which will be used for synchronization in case of need
    - Use loopback devices only
    - Data should be encrypted

# Real data storage (25%)
- Export iSCSI device from PM to VM-B (access from VM-A should be forbidden)
- Mount it on VM-B and create RAID level 1 which will contain iSCSI device and local (inside virtual machine) device

# A bit of security (10%)
- Create encrypted logical volume on VM-A which will contain file password which contains only word quarantine

# Storage (40%)
- Prepare a data server on node delta that serve data for node gamma
- Data should be encrypted
- Use loopback devices as underlying data storage
- Mount data to /mnt/encrypted at node gamma
- Use filesystem that allows you to have more data that is capacity of 'physical' disk
- Prepare a script that will backup these data to node delta (without unmounting)


# Cluster (40%)
- Create a 2-node cluster
- Create NFS mount point that will fail-over to the second node if required
- It is not required to see same data, so shared storage is not required at all
- Mount this NFS (service) on the other node. These two services should not run on on the same machine if possible

# Cluster (30%)
- Create a 2-node cluster
- Create HTTP server which will work as a service
    - web pages should contain the name of node 
    - HTTP server will be accessible from your laptop
    - In a browser you should use same approach to get to web page even if you don't have on which node it is currently running

# A bit of clustering (10%)
- Create a GFS filesystem (not too big) on PM
- Copy image containg this filesystem to VM-B and mount it there
-----

# Bonus track (10%)

- Configure tftp server, so anonymous users can upload files
- https://docs-old.fedoraproject.org/en-US/Fedora/26/html/Installation_Guide/pxe-tftpd.html
- https://llbb.wordpress.com/2007/06/14/installing-tftp-on-fedora/
- https://superuser.com/questions/1046948/how-to-configure-tftp-server-on-fedora-23
- http://www.thelinuxdaily.com/2010/02/guide-on-tftp-server-setup-in-fedora/
