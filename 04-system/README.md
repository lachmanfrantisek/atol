# Exercise #1

## 1. Create unit file:
* After 'start' it will create file `/tmp/hello` with `HELLO` inside.

[![exercise #1](./ex1.gif)](https://asciinema.org/a/172668)


`/etc/systemd/system/hello.service`
```
[Unit]
Description=Say hello

[Service]
Type=oneshot
ExecStart=/bin/bash -c "echo HELLO > /tmp/hello"
RemainAfterExit=true
StandardOutput=journal

[Install]
WantedBy=multi-user.target
```

## 2. Create unit files:
* Create unit files:
    - Listen on port 9999 and active service afterwards
    - Service will write current date to the log


`mydate.socket` (!!!same names!!!)
```
[Unit]
Description=My Date Socket

[Socket]
ListenStream=127.0.0.1:9999
Accept=yes

[Install]
WantedBy=sockets.target
```

`mydate@.service` (!!!same names!!!)
```
[Unit]
Description=My Date Service

[Service]
Type=simple
ExecStart=/usr/bin/date
StandardInput=socket
StandardError=journal
StandardOutput=journal
TimeoutStopSec=5

[Install]
WantedBy=multi-user.target
```


## 3. Create template:
- Based on the first task
- Filename is based on the instance name (@hello) 


`/etc/systemd/system/hello@.service`
```
[Unit]
Description=Say hello to %I

[Service]
Type=oneshot
ExecStart=/bin/bash -c "echo HELLO > /tmp/%i"
RemainAfterExit=true
StandardOutput=journal

[Install]
WantedBy=multi-user.target
```


## 4. Create unit files and templates
- Assemble RAID-5 device from 3 existing files
- Create loopback devices from files using unit templates
- Attempt to do both start/stop properly

`/etc/systemd/system/loopd@.service`
```
[Unit]
Description=Mount loop device %I

[Service]
Type=oneshot
ExecStart=/usr/sbin/losetup /dev/loop%i /discs/d%i
ExecStop=/usr/sbin/losetup -d /dev/loop%i
RemainAfterExit=true
```


`/etc/systemd/system/myraid.service`
```
[Unit]
Description=Create RAID device
Requires=loopd@0.service loopd@1.service loopd@2.service
After=loopd@0.service loopd@1.service loopd@2.service

[Service]
Type=oneshot
ExecStart=/usr/sbin/mdadm --create /dev/md0 --level=5 --raid-devices=3 /dev/loop0 /dev/loop1 /dev/loop2
ExecStop=/bin/bash -c "/usr/sbin/mdadm --stop /dev/md0 && /usr/sbin/mdadm --zero-superblock /dev/loop0 /dev/loop1 /dev/loop2"
RemainAfterExit=true
StandardOutput=journal

[Install]
WantedBy=multi-user.target
```


-----

- [Unit files](https://fedoramagazine.org/systemd-getting-a-grip-on-units/)
- [Template unit files](https://fedoramagazine.org/systemd-template-unit-files/)
- [Unit files on digitalocean](https://www.digitalocean.com/community/tutorials/understanding-systemd-units-and-unit-files)
- [Unit file dosc](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/system_administrators_guide/sect-managing_services_with_systemd-unit_files)
- [Socket unit files](https://gist.github.com/drmalex07/28de61c95b8ba7e5017c)
- [Socket activation (go)](https://vincent.bernat.im/en/blog/2018-systemd-golang-socket-activation)
- [SystemD](https://sachinsharm.wordpress.com/2017/07/30/systemd/)
- [systemctl overview](https://www.tecmint.com/manage-services-using-systemd-and-systemctl-in-linux/)

-----

### Tools

```
semanage fcontext -a -t my_type "/srv/www(/.*)?"
restorecon -v
restorecon -Rv

semanage port

getsebool -a
setsebool

seinfo

chkservice # unit terminal visualiser

ausearch -c 'rpc.statd' --raw | audit2allow -M my-rpcstatd
semodule -X 300 -i my-rpcstatd.pp
```

### Files

```
/etc/sysconfig/selinux
/var/log/audit/audit.log
```

-----


##### Shortcut

```
mkdir /discs
cd /discs
fallocate -l 100M d0
fallocate -l 100M d1
fallocate -l 100M d2
fallocate -l 100M d3
fallocate -l 100M d4
systemctl start loopd@0
systemctl start loopd@1
systemctl start loopd@2
systemctl start loopd@3
systemctl start loopd@4
systemctl enable loopd@0
systemctl enable loopd@1
systemctl enable loopd@2
systemctl enable loopd@3
systemctl enable loopd@4
```

## SELINUX

- [audit2allow](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/security-enhanced_linux/sect-security-enhanced_linux-fixing_problems-allowing_access_audit2allow)
