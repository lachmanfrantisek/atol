# Setup cluster

### Prepare both sides

```
firewall-cmd --permanent --add-service=high-availability
firewall-cmd --reload
systemctl start pcsd.service
systemctl enable pcsd.service

echo "enter_password" | passwd --stdin hacluster
```

### Connection

```
pcs cluster auth alpha beta
pcs cluster setup --name mycluster alpha beta
pcs cluster start --all
pcs status
```

- both:

```
systemctl enable corosync.service
systemctl enable pacemaker.service
```

### Verify corosync

```
corosync-cfgtool -s
```

### Verify pacemaker
```
ps axf

:
:
21028 ?        SLsl   0:01 corosync
21035 ?        Ss     0:00 /usr/sbin/pacemakerd -f
21036 ?        Ss     0:00  \_ /usr/libexec/pacemaker/cib
21037 ?        Ss     0:00  \_ /usr/libexec/pacemaker/stonithd
21038 ?        Ss     0:00  \_ /usr/libexec/pacemaker/lrmd
21039 ?        Ss     0:00  \_ /usr/libexec/pacemaker/attrd
21040 ?        Ss     0:00  \_ /usr/libexec/pacemaker/pengine
21041 ?        Ss     0:00  \_ /usr/libexec/pacemaker/crmd
```


### Disable stonith

```
pcs property set stonith-enabled=false
crm_verify -L
```


### Resources

```
pcs resource create my-ip ocf:heartbeat:IPaddr2  ip=192.168.121.50 cidr_netmask=32 op monitor interval=30s
```

### Webserver

```
firewall-cmd --permanent --add-service=http
firewall-cmd --reload
```

```
pcs resource create my-web ocf:heartbeat:apache \
    configfile=/etc/httpd/conf/httpd.conf \
    statusurl="http://127.0.0.1/server-status" \
    op monitor interval=20s
pcs constraint colocation add my-web with my-ip INFINITY
pcs constraint order my-ip then my-web
```


### ISCSI stonith

#### Loopback

`/etc/systemd/system/loopd@.service`
```
[Unit]
Description=Mount loop device %I

[Instal]
RequiredBy=iscsi.service iscsid.service

[Service]
Type=oneshot
ExecStart=/usr/sbin/losetup /dev/loop%i /discs/d%i
ExecStop=/usr/sbin/losetup -d /dev/loop%i
RemainAfterExit=true
```

```
firewall-cmd --permanent --add-port=3260/tcp

systemctl start target.service
systemctl enable target.service

targetcli
```

##### Mount on both sides

```
cat /etc/iscsi/initiatorname.iscsi

systemctl enable iscsi iscsid.service
systemctl start iscsi iscsid.service

iscsiadm --mode discovery --type sendtargets --portal node-a
iscsiadm -m node -T iqn.2003-01.org.linux-iscsi.node-a.x8664:sn.3938328b2264 -l
iscsiadm -m session
lsblk --scsi
```

```
ls -l /dev/disk/by-id/
```
```
pcs stonith create my-fence-scsi fence_scsi \
    pcmk_host_list="delta gama" \
    pcmk_monitor_action="metadata" \
    pcmk_reboot_action="off" \
    devices="/dev/disk/by-id/wwn-0x60014050c20ac23ea8a45efb505598d2" meta provides="unfencing"
```




------

- [Clusters from Scratch](https://clusterlabs.org/pacemaker/doc/en-US/Pacemaker/1.1/html-single/Clusters_from_Scratch/#_install_the_cluster_software)
- [NFS on cluster](https://www.linuxtechi.com/configure-nfs-server-clustering-pacemaker-centos-7-rhel-7/)
- [another NFS on cluster](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/high_availability_add-on_administration/s1-resourcegroupcreatenfs-haaa)
- [gfs on cluster](https://www.server-world.info/en/note?os=CentOS_7&p=pacemaker&f=3)
