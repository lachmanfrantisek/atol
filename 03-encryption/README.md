

### CryptSetup

#### make, open, close

```
cryptsetup luksFormat /dev/loop1 /discs/key
cryptsetup open -d /discs/key /dev/loop1 d1
cryptsetup close /dev/mapper/d1
```


#### manipulation

```
luksFormat <device> [<new key file>] - formats a LUKS device
luksAddKey <device> [<new key file>] - add key to LUKS device
luksRemoveKey <device> [<key file>] - removes supplied key or key file from LUKS device
luksChangeKey <device> [<key file>] - changes supplied key or key file of LUKS device
luksKillSlot <device> <key slot> - wipes key with number <key slot> from LUKS device
luksDump <device> - dump LUKS partition information
```

- Unit files:

`loopd@.service`
```
[Unit]
Description=Mount loop device %I
Requires=luksd@%i.service

[Service]
Type=oneshot
ExecStart=/usr/sbin/losetup /dev/loop%i /discs/d%i
ExecStop=/usr/sbin/losetup -d /dev/loop%i
RemainAfterExit=true
StandardOutput=journal

[Install]
WantedBy=multi-user.target
```

`luksd@.service`
```
[Unit]
Description=Mount luks device %I
Requires=loopd@%i.service
After=loopd@%i.service

[Service]
Type=oneshot
ExecStart=/bin/bash -c "/usr/sbin/cryptsetup open -d /discs/key /dev/loop%i d%i && /usr/bin/mount /dev/mapper/d%i /mnt/d%i"
ExecStop=/bin/bash -c "usr/bin/umount /mnt/d%i && /usr/sbin/cryptsetup close /dev/mapper/d%i"
RemainAfterExit=true
StandardOutput=journal

[Install]
WantedBy=multi-user.target
```

-----

- [luks with key file](https://www.howtoforge.com/automatically-unlock-luks-encrypted-drives-with-a-keyfile)
- [cryptsetup and luks](https://www.linux.com/blog/how-full-encrypt-your-linux-system-lvm-luks)
- [full disc encryption with btrfs](https://albertodonato.net/blog/posts/full-disk-encryption-with-btrfs-on-ubuntu-xenial.html)
