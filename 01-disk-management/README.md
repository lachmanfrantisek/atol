# Exercise #1

1. Create a loopback device with 100MiB
2. Create GPT on that device with 5 small partitions
3. Copy MBR partition table (*)
   * partitions 1,3,5 from GPT version should be visible
   * MBR/GPT have to be supported mostly on dual/triple boot systems - it is not a preferred setup

[![exercise #1](./ex1.gif)](https://asciinema.org/a/167398)

# Exercise #2

1. Create a physical volume on loopback device
2. Create a filesystem which fill 100% of LV
3. Do not unmount filesystem in next steps
4. Create new physical volume
5. Extend volume group to use also this free space
6. Remove the first loopback device from systems

[![exercise #2](./ex2.gif)](https://asciinema.org/a/167413)

# Exercise #3

1. Create 200MB thin pool
2. How large filesystem it can handle?
3. Remove all thin volumes
4. Create 2x thin volumes (100MB fits on each of them)
5. Create a snapshot of thin volume
6. Create a snapshot of the snapshot
7. Merge snapshot of snapshot to original thin volume

[![exercise #3](./ex3.gif)](https://asciinema.org/a/167414)

