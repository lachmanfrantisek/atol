# Exercise #1

- You will need two installed nodes (A,B)
- Setup networking between nodes
- Node A should act as SAN for node B
    - export 250MB unit with filesystem
- Mount exported device on node B
- Create a standard filesystem
- Try to mount same fs as read-only from other node

[![exercise #1](./ex1.gif)](https://asciinema.org/a/173133)


```
firewall-cmd --permanent --add-port=3260/tcp
```

### Mount:

```
cat /etc/iscsi/initiatorname.iscsi

systemctl enable iscsi iscsid.service
systemctl start iscsi iscsid.service

iscsiadm --mode discovery --type sendtargets --portal node-a
iscsiadm -m node -T iqn.2003-01.org.linux-iscsi.node-a.x8664:sn.3938328b2264 -l
iscsiadm -m session
lsblk --scsi
```

```
systemctl start target.service
systemctl enable target.service
```

### Selinux problems:

```
semodule -DB
cat /var/log/audit/audit.log | audit2allow -a -M mycertwatch
semodule -i mycertwatch.pp
semodule -B
```



-----


- [Tutorial on root.cz](https://www.root.cz/clanky/cluster-na-linuxu-sdilene-uloziste-pomoci-iscsi-a-multipathingu/nazory/)